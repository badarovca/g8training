import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UsersProvider } from '../providers/users/users';
import { Subject } from 'rxjs/Subject';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  pages: Array<{ title: string, component: any, active: boolean, icon: string }>;
  activePage = new Subject();

  @ViewChild(Nav) nav: Nav;

  rootPage:any = 'LoginPage';

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private usersProvider: UsersProvider) {
    this.initializeApp(usersProvider);

    this.pages = [
      { title: 'Home', component: 'HomePage', active: true, icon: 'home' },
      { title: 'Perfil', component: 'PerfilPage', active: false, icon: 'person' },
      { title: 'Sobre', component: 'SobrePage', active: false, icon: 'information-circle' },
      { title: 'Sair', component: 'Logout', active: false, icon: 'exit' },
    ];    
  }

  initializeApp(usersProvider: UsersProvider) {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      usersProvider.authState.subscribe(user => {
        if (user) {
          this.rootPage = 'HomePage';
        } else {
          this.rootPage = 'LoginPage';
        }
      })      
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.component === 'Logout') {
      this.usersProvider.signOut();
    } else {
      this.nav.setRoot(page.component);
      this.activePage.next(page);
    }
  }

}
