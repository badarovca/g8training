import { UsersProvider } from '../../providers/users/users';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Subscription } from 'rxjs/Rx';

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  nickname;
  nicknameSubscription: Subscription;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private usersProvider: UsersProvider,
    private toastCtrl: ToastController) {
    
  }

  ionViewDidLoad() {
    this.nicknameSubscription = this.usersProvider.getCurrentUserNickname().subscribe(nickname => {
      this.nickname = nickname;
    });
  }

  updateNickname() {
    this.usersProvider.updateNickname(this.nickname).then(() => {
      let toast = this.toastCtrl.create({
        duration: 3000,
        message: 'Nome modificado com sucesso!'
      });
      toast.present();
    })
  }

  ionViewWillInload() {
    this.nicknameSubscription.unsubscribe();
  }
}
