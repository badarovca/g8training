import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RodaDaVidaPage } from './roda-da-vida';

@NgModule({
  declarations: [
    RodaDaVidaPage,
  ],
  imports: [
    IonicPageModule.forChild(RodaDaVidaPage),
  ],
})
export class RodaDaVidaPageModule {}
