import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-objetivo',
  templateUrl: 'objetivo.html',
})
export class ObjetivoPage {
  iconMarcado = 'checkbox-outline';
  iconDesmarcado = 'square-outline';
  iconDeclaracao;
  iconMeta;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.iconDeclaracao = this.iconMarcado;
    this.iconMeta = this.iconDesmarcado;
  }

  navigateToPage(pageName): void {
    this.navCtrl.push(pageName);
  }

  navigateToBack(): void {
    this.navCtrl.pop();
  }
}
