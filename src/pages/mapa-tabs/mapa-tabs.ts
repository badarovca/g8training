import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-mapa-tabs',
  templateUrl: 'mapa-tabs.html',
})
export class MapaTabsPage {

  tab1Root: string;
  tab2Root: string;
  tab3Root: string;
  tab4Root: string;
  tab5Root: string;

  constructor() {
    this.tab1Root = 'Mapa1MesPage';
    this.tab2Root = 'Mapa2MesesPage';
    this.tab3Root = 'Mapa3MesesPage';
    this.tab4Root = 'Mapa4MesesPage';
    this.tab5Root = 'Mapa5MesesPage';
  }

}