import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapaTabsPage } from './mapa-tabs';

@NgModule({
  declarations: [
    MapaTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(MapaTabsPage),
  ],
})
export class MapaTabsPageModule {}
