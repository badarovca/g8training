import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GestaoDePontosFracosPage } from './gestao-de-pontos-fracos';

@NgModule({
  declarations: [
    GestaoDePontosFracosPage,
  ],
  imports: [
    IonicPageModule.forChild(GestaoDePontosFracosPage),
  ],
})
export class GestaoDePontosFracosPageModule {}
