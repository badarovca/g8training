import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-modal-jogo-dos-numeros',
  templateUrl: 'modal-jogo-dos-numeros.html',
})
export class ModalJogoDosNumerosPage {

  @ViewChild('slider') slider: Slides;
  slideIndex = 0;
  slides = [
    {
      title: 'Jogo dos Números',
      description: '...',
      img: "assets/imgs/voz.png",   
    }
  ];

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController) { }

  onSlideChanged() {
    this.slideIndex = this.slider.getActiveIndex();
    console.log('Slide changed! Current index is', this.slideIndex);
  }

  goToApp() {
    this.dismiss();
    console.log('Go to App clicked');
  }

  skip() {
    this.dismiss();
    console.log('Skip clicked');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }  
}
