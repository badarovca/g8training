import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MetaPrincipalPage } from './meta-principal';

@NgModule({
  declarations: [
    MetaPrincipalPage,
  ],
  imports: [
    IonicPageModule.forChild(MetaPrincipalPage),
  ],
})
export class MetaPrincipalPageModule {}
