import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Item, ItemSliding, AlertController } from 'ionic-angular';

import { App } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-mapa2-meses',
  templateUrl: 'mapa2-meses.html',
})
export class Mapa2MesesPage {

  activeItemSliding: ItemSliding = null;
  
  items = [ ];  

  constructor(private app: App, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  navigateToPage(pageName): void {
    this.navCtrl.push(pageName);
  }

  navigateToBack(): void {
    const root = this.app.getRootNav();
    root.popToRoot();
  }


  addItem() {
    let prompt = this.alertCtrl.create({
      title: 'Adicionar',
      message: "Qual nova atividade?",
      inputs: [
        {
          name: 'nome',
          placeholder: 'Nome'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Ok',
          handler: data => {
            this.items.push({ title: data.nome });
          }
        }
      ]
    });
    prompt.present();
  }

  deleteItem(list, index) {
    list.splice(index,1);
  }

  openOption(itemSlide: ItemSliding, item: Item, event) {
    event.stopPropagation(); // here if you want item to be tappable
    if (this.activeItemSliding) { // use this so that only one active sliding item allowed
      this.closeOption();
    }

    this.activeItemSliding = itemSlide;
    const swipeAmount = 33; // set your required swipe amount
    
    itemSlide.startSliding(swipeAmount);
    itemSlide.moveSliding(swipeAmount);

    itemSlide.setElementClass('active-slide', true);
    itemSlide.setElementClass('active-options-right', true);
    item.setElementStyle('transition', null);
    item.setElementStyle('transform', 'translate3d(-' + swipeAmount + 'px, 0px, 0px)');
  }

  closeOption() {
    if (this.activeItemSliding) {
      this.activeItemSliding.close();
      this.activeItemSliding = null;
    }
  }

  isBlank(str) {
    return (!str || /^\s*$/.test(str));
  }
    
}