import { UserCredentials, UsersProvider } from '../../providers/users/users';
import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  
  splash = true;

  creds = new UserCredentials();

  constructor(public navCtrl: NavController,
    private usersProvider: UsersProvider,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.splash = false;
    }, 4000);
  }

  login() {
    this.usersProvider.signIn(this.creds).then(() => {

    }, err => {
      let alert = this.alertCtrl.create({
        title: 'Erro!',
        message: err.message,
        buttons: ['OK']
      });
      alert.present();
    })
  }

  openReset() {
    let inputAlert = this.alertCtrl.create({
      title: 'Reset Password',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Resetar',
          handler: data => {
            this.resetPw(data.email);
          }
        }
      ]
    });
    inputAlert.present();
  }

  resetPw(email) {
    this.usersProvider.resetPw(email).then(res => {
      let toast = this.toastCtrl.create({
        duration: 3000,
        message: 'Sucesso! Olhe seu email para mais informações.'
      });
      toast.present();
    }, err => {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: err.message,
        buttons: ['OK']
      });
      alert.present();
    });
  }

  register() {
    this.navCtrl.push('RegisterPage');
  }

}
