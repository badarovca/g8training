import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-slide-walkthrough',
  templateUrl: 'slide-walkthrough.html',
})
export class SlideWalkthroughPage {

  @ViewChild('slider') slider: Slides;
  slideIndex = 0;
  slides = [
    {
      title: 'Aumente Seu Potencial Comunicativo',
      description: 'O teste a seguir ajudará você a descobrir seu sistema de comunicação dominante',
      img: "assets/imgs/voz.png",   
    },
    {
      description: 'Escolha 20 palavras na lista a seguir, por qualquer razão',
      img: "assets/imgs/escutar.png", 
    },
    {
      description: 'Podem ser marcadas as que mais o impressionam ou as que mais se destacam na sua percepção',
      img: "assets/imgs/visao.png", 
    }
  ];

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController) { }

  onSlideChanged() {
    this.slideIndex = this.slider.getActiveIndex();
    console.log('Slide changed! Current index is', this.slideIndex);
  }

  goToApp() {
    this.dismiss();
    console.log('Go to App clicked');
  }

  skip() {
    this.dismiss();
    console.log('Skip clicked');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }  
}
