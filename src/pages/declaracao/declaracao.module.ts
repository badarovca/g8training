import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeclaracaoPage } from './declaracao';

@NgModule({
  declarations: [
    DeclaracaoPage,
  ],
  imports: [
    IonicPageModule.forChild(DeclaracaoPage),
  ],
})
export class DeclaracaoPageModule {}
