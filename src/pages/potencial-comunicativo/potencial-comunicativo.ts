import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-potencial-comunicativo',
  templateUrl: 'potencial-comunicativo.html',
})
export class PotencialComunicativoPage {
  atitudes1: Array<{ descricao: string, perfil: number, marcada: boolean }>;
  atitudes2: Array<{ descricao: string, perfil: number, marcada: boolean }>;
  count: number = 20;
  botao: string;
  countValores: Array<{ perfil: number, count: number, descricao: string }>;
  persona: string = '';

  expanded: any;
  contracted: any;
  showIcon = true;
  preload = true;  

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {
  }

  buildAtitudes() {
    this.atitudes1 = [
      { descricao: 'Trovejar', perfil: 1, marcada: false },
      { descricao: 'Retrato', perfil: 0, marcada: false },
      { descricao: 'Mordida', perfil: 2, marcada: false },
      { descricao: 'Desafinado', perfil: 1, marcada: false },
      { descricao: 'Auréola', perfil: 0, marcada: false },
      { descricao: 'Mexer', perfil: 2, marcada: false },
      { descricao: 'Emoção', perfil: 2, marcada: false },
      { descricao: 'Trombeta', perfil: 1, marcada: false },
      { descricao: 'Aparência', perfil: 0, marcada: false },
      { descricao: 'Miragem', perfil: 0, marcada: false },
      { descricao: 'Grunhido', perfil: 1, marcada: false },
      { descricao: 'Ventania', perfil: 2, marcada: false },
      { descricao: 'Conforto', perfil: 2, marcada: false },
      { descricao: 'Audiência', perfil: 1, marcada: false },
      { descricao: 'Desbotado', perfil: 0, marcada: false },
      { descricao: 'Coceira', perfil: 2, marcada: false },

      { descricao: 'Ruborizar', perfil: 0, marcada: false },
      { descricao: 'Palpável', perfil: 2, marcada: false },
      { descricao: 'Iluminação', perfil: 0, marcada: false },
      { descricao: 'Doçura', perfil: 2, marcada: false },
      { descricao: 'Eco', perfil: 1, marcada: false },
      { descricao: 'Transparecer', perfil: 0, marcada: false },
      { descricao: 'Timbre', perfil: 1, marcada: false },
      { descricao: 'Focalizar', perfil: 0, marcada: false },
      { descricao: 'Perfume', perfil: 2, marcada: false },
      { descricao: 'Ofuscar', perfil: 0, marcada: false },
      { descricao: 'Barulho', perfil: 1, marcada: false },
      { descricao: 'Panorama', perfil: 0, marcada: false },
      { descricao: 'Eloquência', perfil: 1, marcada: false },
      { descricao: 'Periscópio', perfil: 0, marcada: false },
      { descricao: 'Assobio', perfil: 1, marcada: false },
      { descricao: 'Colorir', perfil: 0, marcada: false },
      { descricao: 'Campainha', perfil: 1, marcada: false }
    ];

    this.atitudes2 = [
      { descricao: 'Mergulhar', perfil: 2, marcada: false },
      { descricao: 'Discurso', perfil: 1, marcada: false },
      { descricao: 'Queimadura', perfil: 2, marcada: false },
      { descricao: 'Murmurar', perfil: 1, marcada: false },
      { descricao: 'Saboroso', perfil: 2, marcada: false },
      { descricao: 'Gesticular', perfil: 2, marcada: false },
      { descricao: 'Espinho', perfil: 2, marcada: false },
      { descricao: 'Estampa', perfil: 0, marcada: false },
      { descricao: 'Sensação', perfil: 2, marcada: false },
      { descricao: 'Sotaque', perfil: 1, marcada: false },
      { descricao: 'Visualização', perfil: 0, marcada: false },
      { descricao: 'Aroma', perfil: 2, marcada: false },
      { descricao: 'Ritmo', perfil: 2, marcada: false },
      { descricao: 'Retórica', perfil: 1, marcada: false },
      { descricao: 'Gorjeio', perfil: 1, marcada: false },
      { descricao: 'Sintonizar', perfil: 1, marcada: false },

      { descricao: 'Áspero', perfil: 2, marcada: false },
      { descricao: 'Pálido', perfil: 0, marcada: false },
      { descricao: 'Vozerio', perfil: 1, marcada: false },
      { descricao: 'Veludo', perfil: 2, marcada: false },
      { descricao: 'Claridade', perfil: 0, marcada: false },
      { descricao: 'Observar', perfil: 0, marcada: false },
      { descricao: 'Silêncio', perfil: 1, marcada: false },
      { descricao: 'Arrancar', perfil: 2, marcada: false },
      { descricao: 'Brilhante', perfil: 0, marcada: false },
      { descricao: 'Orquestra', perfil: 1, marcada: false },
      { descricao: 'Paisagem', perfil: 0, marcada: false },
      { descricao: 'Textura', perfil: 2, marcada: false },
      { descricao: 'Acústico', perfil: 1, marcada: false },
      { descricao: 'Segurar', perfil: 2, marcada: false },
      { descricao: 'Espelho', perfil: 0, marcada: false },
      { descricao: 'Sinfonia', perfil: 1, marcada: false },
      { descricao: 'Cenário', perfil: 0, marcada: false }
    ];
  }

  buildValores() {
    this.countValores = [
      { perfil: 0, count: 0, descricao: 'Visual' },
      { perfil: 1, count: 0, descricao: 'Auditivo' },
      { perfil: 2, count: 0, descricao: 'Cinestésico' }
    ];
  }

  updateCount(atitude) {
    atitude.marcada = !atitude.marcada;
    if (atitude.marcada) {
      this.count--;
    } else {
      this.count++;
    }
    this.updateBotao();
  }

  isDisable(checkbox: boolean): boolean {
    if (checkbox) {
      return false;
    } else {
      if (this.count == 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  updateBotao() {
    if (this.count == 0) {
      this.botao = "Salvar";
    } else if (this.count == 1) {
      this.botao = "Falta 1...";
    } else {
      this.botao = "Faltam " + this.count + "...";
    }
  }

  canSalvar() {
    return this.count == 0;
  }

  salvar() {
    this.buildValores();
    for (let atitude of this.atitudes1) {
      if (atitude.marcada) {
        this.countValores[atitude.perfil].count++;
      }
    }
    for (let atitude of this.atitudes2) {
      if (atitude.marcada) {
        this.countValores[atitude.perfil].count++;
      }
    }

    this.countValores.sort(function (a, b) {
      if (a.count < b.count) {
        return 1;
      }
      if (a.count > b.count) {
        return -1;
      }
      return 0;
    });

    if ((this.countValores[0].count - this.countValores[1].count) >= 2) {
      this.persona = this.countValores[0].descricao;
    } else {
      this.persona = this.countValores[0].descricao + ' e ' + this.countValores[1].descricao;
    }

    const alert = this.alertCtrl.create({
      title: 'Você é,',
      subTitle: this.persona + '!',
      buttons: ['Ok']
    });
    alert.present();
  }

  ionViewDidLoad() {
    this.expand();
    this.buildAtitudes();
    this.buildValores();    
    this.updateBotao();
  }

  expand() {
    this.expanded = true;
    this.contracted = !this.expanded;
    this.showIcon = false;
    setTimeout(() => {
      const modal = this.modalCtrl.create('SlideWalkthroughPage');
      modal.onDidDismiss(data => {
        this.expanded = false;
        this.contracted = !this.expanded;
        setTimeout(() => this.showIcon = true, 330);
      });
      modal.present();
    }, 200);
  }  

}
