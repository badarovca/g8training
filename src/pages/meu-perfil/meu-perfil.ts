import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-meu-perfil',
  templateUrl: 'meu-perfil.html',
})
export class MeuPerfilPage {
  iconMarcado = 'checkbox-outline';
  iconDesmarcado = 'square-outline';
  iconQuestionario;  

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.iconQuestionario = this.iconDesmarcado;
  }

  navigateToPage(pageName): void {
    this.navCtrl.push(pageName);
  }

  navigateToBack(): void {
    this.navCtrl.pop();
  }

}
