import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-gestao-de-tempo',
  templateUrl: 'gestao-de-tempo.html',
})
export class GestaoDeTempoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  navigateToPage(pageName): void {
    this.navCtrl.push(pageName);
  }

  navigateToBack(): void {
    this.navCtrl.pop();
  }

}
