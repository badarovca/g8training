webpackJsonp([38],{

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserCredentials; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UsersProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_auth__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(78);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserCredentials = (function () {
    function UserCredentials() {
        this.nickname = '';
        this.email = '';
        this.password = '';
    }
    return UserCredentials;
}());

var UsersProvider = (function () {
    function UsersProvider(http, afAuth, db) {
        this.http = http;
        this.afAuth = afAuth;
        this.db = db;
        this.authState = this.afAuth.authState;
        this.usersRef = this.db.list('users');
        this.g8Ref = this.db.list("g8");
        this.modulo1Ref = this.db.list("modulo1");
        this.m2QuestionarioRef = this.db.list("m2Questionario");
        this.m3MapaDeTalentosRef = this.db.list("m3MapaDeTalentos");
    }
    UsersProvider.prototype.signUp = function (credentials) {
        var _this = this;
        return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password)
            .then(function () {
            return _this.usersRef.set(_this.afAuth.auth.currentUser.uid, {
                email: credentials.email,
                nickname: credentials.nickname
            }), _this.g8Ref.set(_this.afAuth.auth.currentUser.uid, {
                criador: credentials.email
            }), _this.modulo1Ref.set(_this.afAuth.auth.currentUser.uid, {
                declaracao: '',
                meta: '',
                hasDeclaracao: false,
                hasMeta: false
            }), _this.m2QuestionarioRef.set(_this.afAuth.auth.currentUser.uid, {
                naoSei: ''
            }), _this.m3MapaDeTalentosRef.set(_this.afAuth.auth.currentUser.uid, {
                naoSei: ''
            });
        });
    };
    UsersProvider.prototype.newG8 = function () {
        //return this.db.object('g8/' + '').valueChanges();
        //return this.db.list('g8/').set('qqoCCFd8o3YmNaGy6wVQxNiCBPo2', { creator: 'z@z.com', num: '2' });
        //return this.db.list('g8/qqoCCFd8o3YmNaGy6wVQxNiCBPo2').push({ creator: 'z@z.com', num: '2' });
        //return this.db.list('/g8/-L3O8oiTc37hHUkDlOWy/modulo1').push({ declaracao: null, num: null });
    };
    UsersProvider.prototype.newModulo1 = function () {
        return this.db.list('modulo1/').set('-L3O8oiTc37hHUkDlOWy', { declaracao: '', meta: '', id: '-L3O8oiTc37hHUkDlOWy' });
    };
    UsersProvider.prototype.newStruct = function () {
    };
    UsersProvider.prototype.hasObjetivoDeclaracao = function () {
        return true;
    };
    UsersProvider.prototype.signIn = function (credentials) {
        return this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
    };
    UsersProvider.prototype.signOut = function () {
        this.afAuth.auth.signOut();
    };
    UsersProvider.prototype.resetPw = function (email) {
        return this.afAuth.auth.sendPasswordResetEmail(email);
    };
    UsersProvider.prototype.getCurrentUserMail = function () {
        return this.getUserMail(this.afAuth.auth.currentUser.uid);
    };
    UsersProvider.prototype.getUserMail = function (id) {
        return this.db.object('users/' + id + '/email').valueChanges();
    };
    UsersProvider.prototype.getCurrentUserNickname = function () {
        return this.getUserNickname(this.afAuth.auth.currentUser.uid);
    };
    UsersProvider.prototype.getUserNickname = function (id) {
        return this.db.object('users/' + id + '/nickname').valueChanges();
    };
    UsersProvider.prototype.updateNickname = function (newName) {
        return this.db.object('users/' + this.afAuth.auth.currentUser.uid).update({ nickname: newName });
    };
    UsersProvider.prototype.getCurrentDeclaracao = function () {
        return this.getDeclaracao(this.afAuth.auth.currentUser.uid);
    };
    UsersProvider.prototype.getDeclaracao = function (id) {
        return this.db.object('modulo1/' + id + '/declaracao').valueChanges();
    };
    UsersProvider.prototype.updateDeclaracao = function (newDeclaracao) {
        return this.db.object('modulo1/' + this.afAuth.auth.currentUser.uid).update({ declaracao: newDeclaracao });
    };
    UsersProvider.prototype.getCurrentMeta = function () {
        return this.getMeta(this.afAuth.auth.currentUser.uid);
    };
    UsersProvider.prototype.getMeta = function (id) {
        return this.db.object('modulo1/' + id + '/meta').valueChanges();
    };
    UsersProvider.prototype.updateMeta = function (newMeta) {
        return this.db.object('modulo1/' + this.afAuth.auth.currentUser.uid).update({ meta: newMeta });
    };
    UsersProvider.prototype.metodo = function () {
    };
    return UsersProvider;
}());
UsersProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */],
        __WEBPACK_IMPORTED_MODULE_0_angularfire2_auth__["a" /* AngularFireAuth */],
        __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */]])
], UsersProvider);

//# sourceMappingURL=users.js.map

/***/ }),

/***/ 146:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 146;

/***/ }),

/***/ 190:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/calendar/calendar.module": [
		449,
		37
	],
	"../pages/chat/chat.module": [
		450,
		2
	],
	"../pages/comunicacao-pessoal/comunicacao-pessoal.module": [
		451,
		36
	],
	"../pages/dash-meta-principal/dash-meta-principal.module": [
		452,
		35
	],
	"../pages/declaracao/declaracao.module": [
		453,
		34
	],
	"../pages/diario-de-bordo/diario-de-bordo.module": [
		454,
		33
	],
	"../pages/gestao-de-pontos-fracos/gestao-de-pontos-fracos.module": [
		455,
		32
	],
	"../pages/gestao-de-tempo/gestao-de-tempo.module": [
		456,
		31
	],
	"../pages/home/home.module": [
		457,
		30
	],
	"../pages/jogo-dos-numeros/jogo-dos-numeros.module": [
		458,
		29
	],
	"../pages/login/login.module": [
		459,
		28
	],
	"../pages/mapa-tabs/mapa-tabs.module": [
		460,
		27
	],
	"../pages/mapa1-mes/mapa1-mes.module": [
		461,
		26
	],
	"../pages/mapa2-meses/mapa2-meses.module": [
		462,
		25
	],
	"../pages/mapa3-meses/mapa3-meses.module": [
		463,
		24
	],
	"../pages/mapa4-meses/mapa4-meses.module": [
		464,
		23
	],
	"../pages/mapa5-meses/mapa5-meses.module": [
		465,
		22
	],
	"../pages/mensagens/mensagens.module": [
		466,
		21
	],
	"../pages/meta-principal/meta-principal.module": [
		467,
		20
	],
	"../pages/meu-perfil/meu-perfil.module": [
		468,
		19
	],
	"../pages/modal-jogo-dos-numeros/modal-jogo-dos-numeros.module": [
		469,
		18
	],
	"../pages/modal-questionario/modal-questionario.module": [
		470,
		17
	],
	"../pages/modulos/modulos.module": [
		471,
		16
	],
	"../pages/objetivo/objetivo.module": [
		472,
		15
	],
	"../pages/perfil/perfil.module": [
		473,
		14
	],
	"../pages/placar/placar.module": [
		474,
		1
	],
	"../pages/planejamento/planejamento.module": [
		475,
		13
	],
	"../pages/popup-fab/popup-fab-modal/popup-fab-modal.module": [
		476,
		12
	],
	"../pages/popup-fab/popup-fab.module": [
		477,
		11
	],
	"../pages/potencial-comunicativo/modal-potencial/modal-potencial.module": [
		478,
		10
	],
	"../pages/potencial-comunicativo/potencial-comunicativo.module": [
		479,
		9
	],
	"../pages/questionario/questionario.module": [
		480,
		8
	],
	"../pages/register/register.module": [
		481,
		7
	],
	"../pages/roda-da-vida/roda-da-vida.module": [
		482,
		0
	],
	"../pages/slide-walkthrough/slide-walkthrough.module": [
		483,
		6
	],
	"../pages/sobre/sobre.module": [
		484,
		5
	],
	"../pages/test/test.module": [
		485,
		4
	],
	"../pages/teste2/teste2.module": [
		486,
		3
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 190;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmojiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/*
  Generated class for the EmojiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var EmojiProvider = (function () {
    function EmojiProvider() {
    }
    EmojiProvider.prototype.getEmojis = function () {
        var EMOJIS = "😀 😃 😄 😁 😆 😅 😂 🤣 ☺️ 😊 😇 🙂 🙃 😉 😌 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 🤡 🤠 😏 😒 😞 😔 😟 😕 🙁" +
            " ☹️ 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 🤤 😭 😓 😪 😴 🙄 🤔 🤥 😬 🤐 🤢 🤧 😷 🤒 🤕 😈 👿" +
            " 👹 👺 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊ 🤛 🤜 🤞 ✌️ 🤘 👌 👈 👉 👆 👇 ☝️ ✋ 🤚" +
            " 🖐 🖖 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 👶 👦 👧 👨 👩 👱‍♀️ 👱 👴 👵 👲 👳‍♀️ 👳 👮‍♀️ 👮 👷‍♀️ 👷" +
            " 💂‍♀️ 💂 🕵️‍♀️ 🕵️ 👩‍⚕️ 👨‍⚕️ 👩‍🌾 👨‍🌾 👩‍🍳 👨‍🍳 👩‍🎓 👨‍🎓 👩‍🎤 👨‍🎤 👩‍🏫 👨‍🏫 👩‍🏭 👨‍🏭 👩‍💻 👨‍💻 👩‍💼 👨‍💼 👩‍🔧 👨‍🔧 👩‍🔬 👨‍🔬" +
            " 👩‍🎨 👨‍🎨 👩‍🚒 👨‍🚒 👩‍✈️ 👨‍✈️ 👩‍🚀 👨‍🚀 👩‍⚖️ 👨‍⚖️ 🤶 🎅 👸 🤴 👰 🤵 👼 🤰 🙇‍♀️ 🙇 💁 💁‍♂️ 🙅 🙅‍♂️ 🙆 🙆‍♂️ 🙋 🙋‍♂️ 🤦‍♀️ 🤦‍♂️ 🤷‍♀" +
            "️ 🤷‍♂️ 🙎 🙎‍♂️ 🙍 🙍‍♂️ 💇 💇‍♂️ 💆 💆‍♂️ 🕴 💃 🕺 👯 👯‍♂️ 🚶‍♀️ 🚶 🏃‍♀️ 🏃 👫 👭 👬 💑 👩‍❤️‍👩 👨‍❤️‍👨 💏 👩‍❤️‍💋‍👩 👨‍❤️‍💋‍👨 👪 👨‍👩‍👧" +
            " 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👩‍👩‍👦 👩‍👩‍👧 👩‍👩‍👧‍👦 👩‍👩‍👦‍👦 👩‍👩‍👧‍👧 👨‍👨‍👦 👨‍👨‍👧 👨‍👨‍👧‍👦 👨‍👨‍👦‍👦 👨‍👨‍👧‍👧 👩‍👦 👩‍👧" +
            " 👩‍👧‍👦 👩‍👦‍👦 👩‍👧‍👧 👨‍👦 👨‍👧 👨‍👧‍👦 👨‍👦‍👦 👨‍👧‍👧 👚 👕 👖 👔 👗 👙 👘 👠 👡 👢 👞 👟 👒 🎩 🎓 👑 ⛑ 🎒 👝 👛 👜 💼 👓" +
            " 🕶 🌂 ☂️";
        var EmojiArr = EMOJIS.split(' ');
        var groupNum = Math.ceil(EmojiArr.length / (24));
        var items = [];
        for (var i = 0; i < groupNum; i++) {
            items.push(EmojiArr.slice(i * 24, (i + 1) * 24));
        }
        return items;
    };
    return EmojiProvider;
}());
EmojiProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], EmojiProvider);

//# sourceMappingURL=emoji.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(302);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_firebase_config__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_emoji__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_chats_chats__ = __webpack_require__(448);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_users_users__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_ionic2_calendar__ = __webpack_require__(284);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {
                backButtonText: ''
            }, {
                links: [
                    { loadChildren: '../pages/calendar/calendar.module#CalendarPageModule', name: 'CalendarPage', segment: 'calendar', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/comunicacao-pessoal/comunicacao-pessoal.module#ComunicacaoPessoalPageModule', name: 'ComunicacaoPessoalPage', segment: 'comunicacao-pessoal', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/dash-meta-principal/dash-meta-principal.module#DashMetaPrincipalPageModule', name: 'DashMetaPrincipalPage', segment: 'dash-meta-principal', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/declaracao/declaracao.module#DeclaracaoPageModule', name: 'DeclaracaoPage', segment: 'declaracao', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/diario-de-bordo/diario-de-bordo.module#DiarioDeBordoPageModule', name: 'DiarioDeBordoPage', segment: 'diario-de-bordo', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/gestao-de-pontos-fracos/gestao-de-pontos-fracos.module#GestaoDePontosFracosPageModule', name: 'GestaoDePontosFracosPage', segment: 'gestao-de-pontos-fracos', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/gestao-de-tempo/gestao-de-tempo.module#GestaoDeTempoPageModule', name: 'GestaoDeTempoPage', segment: 'gestao-de-tempo', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/home/home.module#HomeModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/jogo-dos-numeros/jogo-dos-numeros.module#JogoDosNumerosPageModule', name: 'JogoDosNumerosPage', segment: 'jogo-dos-numeros', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/mapa-tabs/mapa-tabs.module#MapaTabsPageModule', name: 'MapaTabsPage', segment: 'mapa-tabs', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/mapa1-mes/mapa1-mes.module#Mapa1MesPageModule', name: 'Mapa1MesPage', segment: 'mapa1-mes', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/mapa2-meses/mapa2-meses.module#Mapa2MesesPageModule', name: 'Mapa2MesesPage', segment: 'mapa2-meses', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/mapa3-meses/mapa3-meses.module#Mapa3MesesPageModule', name: 'Mapa3MesesPage', segment: 'mapa3-meses', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/mapa4-meses/mapa4-meses.module#Mapa4MesesPageModule', name: 'Mapa4MesesPage', segment: 'mapa4-meses', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/mapa5-meses/mapa5-meses.module#Mapa5MesesPageModule', name: 'Mapa5MesesPage', segment: 'mapa5-meses', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/mensagens/mensagens.module#MensagensPageModule', name: 'MensagensPage', segment: 'mensagens', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/meta-principal/meta-principal.module#MetaPrincipalPageModule', name: 'MetaPrincipalPage', segment: 'meta-principal', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/meu-perfil/meu-perfil.module#MeuPerfilPageModule', name: 'MeuPerfilPage', segment: 'meu-perfil', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/modal-jogo-dos-numeros/modal-jogo-dos-numeros.module#ModalJogoDosNumerosPageModule', name: 'ModalJogoDosNumerosPage', segment: 'modal-jogo-dos-numeros', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/modal-questionario/modal-questionario.module#SlideWalkthroughPageModule', name: 'ModalQuestionarioPage', segment: 'modal-questionario', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/modulos/modulos.module#ModulosPageModule', name: 'ModulosPage', segment: 'modulos', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/objetivo/objetivo.module#ObjetivoPageModule', name: 'ObjetivoPage', segment: 'objetivo', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/perfil/perfil.module#PerfilPageModule', name: 'PerfilPage', segment: 'perfil', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/placar/placar.module#PlacarPageModule', name: 'PlacarPage', segment: 'placar', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/planejamento/planejamento.module#PlanejamentoPageModule', name: 'PlanejamentoPage', segment: 'planejamento', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/popup-fab/popup-fab-modal/popup-fab-modal.module#PopupFabModalPageModule', name: 'PopupFabModalPage', segment: 'popup-fab-modal', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/popup-fab/popup-fab.module#PopupFabPageModule', name: 'PopupFabPage', segment: 'popup-fab', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/potencial-comunicativo/modal-potencial/modal-potencial.module#PopupFabModalPageModule', name: 'ModalPotencialPage', segment: 'modal-potencial', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/potencial-comunicativo/potencial-comunicativo.module#PotencialComunicativoPageModule', name: 'PotencialComunicativoPage', segment: 'potencial-comunicativo', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/questionario/questionario.module#QuestionarioPageModule', name: 'QuestionarioPage', segment: 'questionario', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/roda-da-vida/roda-da-vida.module#RodaDaVidaPageModule', name: 'RodaDaVidaPage', segment: 'roda-da-vida', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/slide-walkthrough/slide-walkthrough.module#SlideWalkthroughPageModule', name: 'SlideWalkthroughPage', segment: 'slide-walkthrough', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/sobre/sobre.module#SobrePageModule', name: 'SobrePage', segment: 'sobre', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/test/test.module#TestPageModule', name: 'TestPage', segment: 'test', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/teste2/teste2.module#Teste2PageModule', name: 'Teste2Page', segment: 'teste2', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_5_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_9__app_firebase_config__["a" /* FIREBASE_CONFIG */]),
            __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__["b" /* AngularFireAuthModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["b" /* AngularFireDatabaseModule */],
            __WEBPACK_IMPORTED_MODULE_15_ionic2_calendar__["a" /* NgCalendarModule */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_12__providers_emoji__["a" /* EmojiProvider */],
            __WEBPACK_IMPORTED_MODULE_13__providers_chats_chats__["a" /* ChatsProvider */],
            __WEBPACK_IMPORTED_MODULE_14__providers_users_users__["b" /* UsersProvider */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["a" /* AngularFireDatabase */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 330:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 438:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_users_users__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, usersProvider) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.usersProvider = usersProvider;
        this.activePage = new __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__["Subject"]();
        this.rootPage = 'LoginPage';
        this.initializeApp(usersProvider);
        this.pages = [
            { title: 'Home', component: 'HomePage', active: true, icon: 'home' },
            { title: 'Perfil', component: 'PerfilPage', active: false, icon: 'person' },
            { title: 'Sobre', component: 'SobrePage', active: false, icon: 'information-circle' },
            { title: 'Sair', component: 'Logout', active: false, icon: 'exit' },
        ];
    }
    MyApp.prototype.initializeApp = function (usersProvider) {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            usersProvider.authState.subscribe(function (user) {
                if (user) {
                    _this.rootPage = 'HomePage';
                }
                else {
                    _this.rootPage = 'LoginPage';
                }
            });
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component === 'Logout') {
            this.usersProvider.signOut();
        }
        else {
            this.nav.setRoot(page.component);
            this.activePage.next(page);
        }
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"D:\Projects\g8training\src\app\app.html"*/'<ion-menu [content]="content">\n\n\n\n  <ion-header>\n\n    <ion-toolbar color="primary">\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button color="primary" menuClose="left" ion-item detail-none *ngFor="let p of pages" (click)="openPage(p)">\n\n        <ion-icon [name]="p.icon" item-left></ion-icon>\n\n        {{p.title}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\Projects\g8training\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_4__providers_users_users__["b" /* UsersProvider */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FIREBASE_CONFIG; });
var FIREBASE_CONFIG = {
    apiKey: "AIzaSyDI_OqrTuTF8Qt1H3ZatsIOitua3bwFSnA",
    authDomain: "g8training-fde92.firebaseapp.com",
    databaseURL: "https://g8training-fde92.firebaseio.com",
    projectId: "g8training-fde92",
    storageBucket: "g8training-fde92.appspot.com",
    messagingSenderId: "819375049127"
};
//# sourceMappingURL=app.firebase.config.js.map

/***/ }),

/***/ 448:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ChatsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ChatsProvider = (function () {
    function ChatsProvider(http) {
        this.http = http;
    }
    return ChatsProvider;
}());
ChatsProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
], ChatsProvider);

//# sourceMappingURL=chats.js.map

/***/ })

},[286]);
//# sourceMappingURL=main.js.map