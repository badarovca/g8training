webpackJsonp([34],{

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeclaracaoPageModule", function() { return DeclaracaoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__declaracao__ = __webpack_require__(675);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DeclaracaoPageModule = (function () {
    function DeclaracaoPageModule() {
    }
    return DeclaracaoPageModule;
}());
DeclaracaoPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__declaracao__["a" /* DeclaracaoPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__declaracao__["a" /* DeclaracaoPage */]),
        ],
    })
], DeclaracaoPageModule);

//# sourceMappingURL=declaracao.module.js.map

/***/ }),

/***/ 675:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeclaracaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_users_users__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DeclaracaoPage = (function () {
    function DeclaracaoPage(navCtrl, usersProvider, toastCtrl) {
        this.navCtrl = navCtrl;
        this.usersProvider = usersProvider;
        this.toastCtrl = toastCtrl;
    }
    DeclaracaoPage.prototype.editarDeclaracao = function () {
        var _this = this;
        this.usersProvider.updateDeclaracao(this.declaracao).then(function () {
            var toast = _this.toastCtrl.create({
                duration: 3000,
                message: 'Declaração editada!'
            });
            toast.present();
        });
    };
    DeclaracaoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.declaracaoSubscription = this.usersProvider.getCurrentDeclaracao().subscribe(function (declaracao) {
            _this.declaracao = declaracao;
        });
    };
    DeclaracaoPage.prototype.navigateToBack = function () {
        this.navCtrl.pop();
    };
    DeclaracaoPage.prototype.podeSalvar = function () {
        if (this.declaracao = "") {
            return false;
        }
        else
            return true;
    };
    return DeclaracaoPage;
}());
DeclaracaoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
        selector: 'page-declaracao',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\declaracao\declaracao.html"*/'<ion-header>\n\n  <ion-navbar color="objetivo">\n\n    <ion-title>Declaração</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content >\n\n  \n\n  <ion-item>\n\n    <ion-label>Escreva sua Declaração abaixo</ion-label>\n\n  </ion-item>\n\n  \n\n  <ion-textarea padding class="campo-declaracao" placeholder="" [(ngModel)]="declaracao"></ion-textarea>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <button padding ion-button full color="objetivo" (click)=\'editarDeclaracao()\'>Editar</button>\n\n</ion-footer>\n\n'/*ion-inline-end:"D:\Projects\g8training\src\pages\declaracao\declaracao.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_0__providers_users_users__["b" /* UsersProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ToastController */]])
], DeclaracaoPage);

//# sourceMappingURL=declaracao.js.map

/***/ })

});
//# sourceMappingURL=34.js.map