webpackJsonp([4],{

/***/ 485:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestPageModule", function() { return TestPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__test__ = __webpack_require__(707);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_calendar__ = __webpack_require__(284);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var TestPageModule = (function () {
    function TestPageModule() {
    }
    return TestPageModule;
}());
TestPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__test__["a" /* TestPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__test__["a" /* TestPage */]),
            __WEBPACK_IMPORTED_MODULE_3_ionic2_calendar__["a" /* NgCalendarModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__test__["a" /* TestPage */]
        ]
    })
], TestPageModule);

//# sourceMappingURL=test.module.js.map

/***/ }),

/***/ 707:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
//import * as moment from 'moment';


/**
 * Generated class for the TestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TestPage = (function () {
    function TestPage(alertCtrl) {
        this.alertCtrl = alertCtrl;
        this.timeInSeconds = 60;
        this.pontos = 0;
    }
    TestPage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    TestPage.prototype.hasFinished = function () {
        return this.finished;
    };
    TestPage.prototype.initTimer = function () {
        if (!this.timeInSeconds) {
            this.timeInSeconds = 0;
        }
        this.seconds = this.timeInSeconds;
        this.runTimer = false;
        this.started = false;
        this.finished = false;
        this.secondsRemaining = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.secondsRemaining);
    };
    TestPage.prototype.startTimer = function () {
        this.started = true;
        this.runTimer = true;
        this.timerTick();
    };
    TestPage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    TestPage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    TestPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.secondsRemaining--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.secondsRemaining);
            if (_this.secondsRemaining > 0) {
                _this.timerTick();
            }
            else {
                _this.finished = true;
            }
        }, 1000);
    };
    TestPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var secNum = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(secNum / 3600);
        var minutes = Math.floor((secNum - (hours * 3600)) / 60);
        var seconds = secNum - (hours * 3600) - (minutes * 60);
        var minutesString = '';
        var secondsString = '';
        minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
        secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
        return minutesString + ':' + secondsString;
    };
    TestPage.prototype.botaoCerto = function () {
        this.pontos = this.pontos + 3;
    };
    TestPage.prototype.botaoErrado = function () {
        this.pontos = this.pontos - 20 + this.secondsRemaining;
        var alert = this.alertCtrl.create({
            title: 'Botão Errado!',
            subTitle: 'Sua pontuação é ' + this.pontos,
            buttons: ['OK']
        });
        alert.present();
        this.pontos = 0;
        this.initTimer();
    };
    TestPage.prototype.terminar = function () {
        var alert = this.alertCtrl.create({
            title: 'Acabou!',
            subTitle: 'Acabou...',
            buttons: ['OK']
        });
        alert.present();
        this.pontos = 0;
    };
    return TestPage;
}());
TestPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-test',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\test\test.html"*/'<ion-header no-border>\n\n  <ion-navbar color="planejamento">\n\n    <ion-title>Teste</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n\n\n  <div>\n\n    <ion-row>\n\n      <ion-col no-padding>\n\n        <div no-padding>\n\n          <button ion-button large block clear class="timer-button x-large">{{displayTime}}</button>\n\n        </div>\n\n      </ion-col>\n\n      <ion-col no-padding>\n\n        <div no-padding>\n\n          <button ion-button large block clear class="timer-button x-large">{{pontos}} PONTOS</button>\n\n        </div>\n\n      </ion-col>\n\n    </ion-row>\n\n  </div>\n\n\n\n  <button padding ion-button full color="primary" (click)=\'startTimer()\'>Iniciar</button>\n\n  <button padding ion-button full color="light" (click)=\'pauseTimer()\'>Pause</button>\n\n  <button padding ion-button full color="perfil" (click)=\'initTimer()\'>Reiniciar</button>\n\n  <br/>\n\n  <button padding ion-button full color="comunicacao" (click)=\'botaoCerto()\'>Botão Correto</button>\n\n  <button padding ion-button full color="danger" (click)=\'botaoErrado()\'>Botão Errado</button>\n\n  <button padding ion-button full color="dark" (click)=\'terminar()\'>Terminar</button>\n\n\n\n</ion-content>\n\n\n\n<!--\n\n<ion-header>\n\n  <ion-navbar color="primary">\n\n    <ion-buttons start>\n\n      <button ion-button icon-only (click)="cancel()">\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>Event Details</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-input type="text" placeholder="Title" [(ngModel)]="event.title"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>Start</ion-label>\n\n      <ion-datetime displayFormat="MM/DD/YYYY HH:mm" pickerFormat="MMM D:HH:mm" [(ngModel)]="event.startTime" [min]="minDate"></ion-datetime>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>End</ion-label>\n\n      <ion-datetime displayFormat="MM/DD/YYYY HH:mm" pickerFormat="MMM D:HH:mm" [(ngModel)]="event.endTime" [min]="minDate"></ion-datetime>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label>All Day?</ion-label>\n\n      <ion-checkbox [(ngModel)]="event.allDay"></ion-checkbox>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <button ion-button full icon-left (click)="save()">\n\n    <ion-icon name="checkmark"></ion-icon> Add Event\n\n  </button>\n\n</ion-content>\n\n-->'/*ion-inline-end:"D:\Projects\g8training\src\pages\test\test.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], TestPage);

//# sourceMappingURL=test.js.map

/***/ })

});
//# sourceMappingURL=4.js.map