webpackJsonp([32],{

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GestaoDePontosFracosPageModule", function() { return GestaoDePontosFracosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gestao_de_pontos_fracos__ = __webpack_require__(677);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GestaoDePontosFracosPageModule = (function () {
    function GestaoDePontosFracosPageModule() {
    }
    return GestaoDePontosFracosPageModule;
}());
GestaoDePontosFracosPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__gestao_de_pontos_fracos__["a" /* GestaoDePontosFracosPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__gestao_de_pontos_fracos__["a" /* GestaoDePontosFracosPage */]),
        ],
    })
], GestaoDePontosFracosPageModule);

//# sourceMappingURL=gestao-de-pontos-fracos.module.js.map

/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GestaoDePontosFracosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the GestaoDePontosFracosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GestaoDePontosFracosPage = (function () {
    function GestaoDePontosFracosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    GestaoDePontosFracosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GestaoDePontosFracosPage');
    };
    return GestaoDePontosFracosPage;
}());
GestaoDePontosFracosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-gestao-de-pontos-fracos',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\gestao-de-pontos-fracos\gestao-de-pontos-fracos.html"*/'<ion-header>\n  <ion-navbar color="fracos">\n    <ion-title>Gestão de Pontos Fracos</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-card class="etapa-card" tappable>\n    <ion-item class="titulo">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <div class="left">\n              <ion-icon class="goTo" name="arrow-dropright-circle" item-left></ion-icon>\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="middle">\n              Pontos Fortes\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="right">\n              <ion-icon class="goTo" [name]="iconDeclaracao" item-left></ion-icon>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-card>\n\n  <ion-card class="etapa-card" tappable>\n    <ion-item class="titulo">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <div class="left">\n              <ion-icon class="goTo" name="arrow-dropright-circle" item-left></ion-icon>\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="middle">\n              Pontos Fracos\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="right">\n              <ion-icon class="goTo" [name]="iconDeclaracao" item-left></ion-icon>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-card>\n\n  <ion-card class="etapa-card" tappable>\n    <ion-item class="titulo">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <div class="left">\n              <ion-icon class="goTo" name="arrow-dropright-circle" item-left></ion-icon>\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="middle">\n              Exercício\n            </div>\n          </ion-col>\n          <ion-col>\n            <div class="right">\n              <ion-icon class="goTo" [name]="iconDeclaracao" item-left></ion-icon>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\Projects\g8training\src\pages\gestao-de-pontos-fracos\gestao-de-pontos-fracos.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], GestaoDePontosFracosPage);

//# sourceMappingURL=gestao-de-pontos-fracos.js.map

/***/ })

});
//# sourceMappingURL=32.js.map