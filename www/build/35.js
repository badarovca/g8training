webpackJsonp([35],{

/***/ 452:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashMetaPrincipalPageModule", function() { return DashMetaPrincipalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dash_meta_principal__ = __webpack_require__(674);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DashMetaPrincipalPageModule = (function () {
    function DashMetaPrincipalPageModule() {
    }
    return DashMetaPrincipalPageModule;
}());
DashMetaPrincipalPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__dash_meta_principal__["a" /* DashMetaPrincipalPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__dash_meta_principal__["a" /* DashMetaPrincipalPage */]),
        ],
    })
], DashMetaPrincipalPageModule);

//# sourceMappingURL=dash-meta-principal.module.js.map

/***/ }),

/***/ 674:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashMetaPrincipalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_users_users__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashMetaPrincipalPage = (function () {
    function DashMetaPrincipalPage(navCtrl, usersProvider) {
        this.navCtrl = navCtrl;
        this.usersProvider = usersProvider;
    }
    DashMetaPrincipalPage.prototype.ionViewWillLoad = function () {
        var _this = this;
        this.metaSubscription = this.usersProvider.getCurrentMeta().subscribe(function (meta) {
            _this.meta = meta;
        });
        this.nomeSubscription = this.usersProvider.getCurrentUserNickname().subscribe(function (nome) {
            _this.nome = nome;
        });
    };
    DashMetaPrincipalPage.prototype.navigateToBack = function () {
        this.navCtrl.pop();
    };
    return DashMetaPrincipalPage;
}());
DashMetaPrincipalPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-dash-meta-principal',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\dash-meta-principal\dash-meta-principal.html"*/'<ion-header no-border>\n\n  <ion-navbar color="primary">\n\n    <ion-title>Meta Principal</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <div class="pai">\n\n    <div class="meta">\n\n      <p>{{meta}}</p>\n\n      <h3>{{nome}}</h3>\n\n    </div>\n\n  </div>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\Projects\g8training\src\pages\dash-meta-principal\dash-meta-principal.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_users_users__["b" /* UsersProvider */]])
], DashMetaPrincipalPage);

//# sourceMappingURL=dash-meta-principal.js.map

/***/ })

});
//# sourceMappingURL=35.js.map