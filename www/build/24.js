webpackJsonp([24],{

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Mapa3MesesPageModule", function() { return Mapa3MesesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mapa3_meses__ = __webpack_require__(685);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Mapa3MesesPageModule = (function () {
    function Mapa3MesesPageModule() {
    }
    return Mapa3MesesPageModule;
}());
Mapa3MesesPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__mapa3_meses__["a" /* Mapa3MesesPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__mapa3_meses__["a" /* Mapa3MesesPage */]),
        ],
    })
], Mapa3MesesPageModule);

//# sourceMappingURL=mapa3-meses.module.js.map

/***/ }),

/***/ 685:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Mapa3MesesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Mapa3MesesPage = (function () {
    function Mapa3MesesPage(app, navCtrl, navParams, alertCtrl) {
        this.app = app;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.activeItemSliding = null;
        this.items = [];
    }
    Mapa3MesesPage.prototype.navigateToPage = function (pageName) {
        this.navCtrl.push(pageName);
    };
    Mapa3MesesPage.prototype.navigateToBack = function () {
        var root = this.app.getRootNav();
        root.popToRoot();
    };
    Mapa3MesesPage.prototype.addItem = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Adicionar',
            message: "Qual nova atividade?",
            inputs: [
                {
                    name: 'nome',
                    placeholder: 'Nome'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    handler: function (data) {
                    }
                },
                {
                    text: 'Ok',
                    handler: function (data) {
                        _this.items.push({ title: data.nome });
                    }
                }
            ]
        });
        prompt.present();
    };
    Mapa3MesesPage.prototype.deleteItem = function (list, index) {
        list.splice(index, 1);
    };
    Mapa3MesesPage.prototype.openOption = function (itemSlide, item, event) {
        event.stopPropagation(); // here if you want item to be tappable
        if (this.activeItemSliding) {
            this.closeOption();
        }
        this.activeItemSliding = itemSlide;
        var swipeAmount = 33; // set your required swipe amount
        itemSlide.startSliding(swipeAmount);
        itemSlide.moveSliding(swipeAmount);
        itemSlide.setElementClass('active-slide', true);
        itemSlide.setElementClass('active-options-right', true);
        item.setElementStyle('transition', null);
        item.setElementStyle('transform', 'translate3d(-' + swipeAmount + 'px, 0px, 0px)');
    };
    Mapa3MesesPage.prototype.closeOption = function () {
        if (this.activeItemSliding) {
            this.activeItemSliding.close();
            this.activeItemSliding = null;
        }
    };
    Mapa3MesesPage.prototype.isBlank = function (str) {
        return (!str || /^\s*$/.test(str));
    };
    return Mapa3MesesPage;
}());
Mapa3MesesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-mapa3-meses',template:/*ion-inline-start:"D:\Projects\g8training\src\pages\mapa3-meses\mapa3-meses.html"*/'<ion-header no-border>\n\n    <ion-navbar  hideBackButton="true" color="planejamento">\n\n      <ion-title>Mapa de Talentos</ion-title>  \n\n      <ion-buttons begin>\n\n          <button ion-button icon-left color="light" (click)="navigateToBack()">\n\n            <ion-icon name="ios-arrow-back"></ion-icon>\n\n          </button>\n\n      </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  \n\n    <ion-fab right top edge (click)="addItem()">\n\n      <button ion-fab class="pop-in" color="primary">\n\n          <ion-icon name="ios-add"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n  \n\n    <ion-fab bottom right >\n\n      <button ion-fab class="pop-in" color="primary">\n\n        <ion-icon  name="ios-information-outline"></ion-icon>\n\n      </button>\n\n      <ion-fab-list side="top">\n\n        <button ion-fab color="primary">Links</button>\n\n        <button ion-fab color="primary">Ajuda</button>\n\n      </ion-fab-list>\n\n    </ion-fab>\n\n  \n\n  \n\n    <h2>3º Mês</h2>\n\n  \n\n    <ion-list>\n\n      <ion-item-sliding *ngFor="let item of items; let i = index; ">\n\n        <ion-item>\n\n          {{item.title}}\n\n          <button ion-button item-right icon-only clear (click)="deleteItem(itemsMes1, i)">\n\n            <ion-icon color="grey" name="trash"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n      </ion-item-sliding>\n\n    </ion-list>  \n\n    \n\n  </ion-content>'/*ion-inline-end:"D:\Projects\g8training\src\pages\mapa3-meses\mapa3-meses.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], Mapa3MesesPage);

//# sourceMappingURL=mapa3-meses.js.map

/***/ })

});
//# sourceMappingURL=24.js.map